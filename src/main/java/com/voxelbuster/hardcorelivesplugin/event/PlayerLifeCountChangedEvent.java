package com.voxelbuster.hardcorelivesplugin.event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerLifeCountChangedEvent extends PlayerEvent {

    private final int lives;

    private static final HandlerList handlers = new HandlerList();

    public PlayerLifeCountChangedEvent(Player respawnPlayer, int livesAfterRespawn) {
        super(respawnPlayer);
        this.lives = livesAfterRespawn;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public int getLives() {
        return lives;
    }
}
