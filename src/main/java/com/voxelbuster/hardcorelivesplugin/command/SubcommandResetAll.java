package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubcommandResetAll extends PluginSubcommand {
    protected SubcommandResetAll(HardcoreLivesPlugin plugin) {
        super("resetAll", plugin);
        this.description = "Resets the player data for every player. This includes offline players.";
        this.usage = "/hl resetAll";
        this.aliases = Collections.singletonList("resetall");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (PermissionUtil.hasPermission(sender, "hardcorelives.resetAll")) {
            if (aliases.contains(alias.toLowerCase())) {
                try {
                    plugin.getConfigManager().wipePlayers(plugin.getPlayersDir());
                    sender.sendMessage(ChatColor.LIGHT_PURPLE + "All Hardcore Lives player data wiped.");
                    plugin.updateScoreboard();
                    return true;
                } catch (IOException e) {
                    plugin.getLogger().severe(ChatColor.RED + "Could not wipe players!");
                    e.printStackTrace();
                    return false;
                }
            }
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            return false;
        }
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return new ArrayList<>();
    }
}
