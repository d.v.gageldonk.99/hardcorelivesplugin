package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubcommandSave extends PluginSubcommand {
    protected SubcommandSave(HardcoreLivesPlugin plugin) {
        super("save", plugin);
        this.description = "Saves all player data for the plugin.";
        this.usage = "/hl save";
        this.aliases = Collections.singletonList("save");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (PermissionUtil.hasPermission(sender, "hardcorelives.save")) {
            if (aliases.contains(alias.toLowerCase())) {
                try {
                    plugin.getConfigManager().saveAllPlayers(plugin.getPlayersDir());
                } catch (IOException e) {
                    sender.sendMessage(ChatColor.RED + "Saving player data failed!");
                    plugin.getLogger().severe("Saving player data failed!");
                    e.printStackTrace();
                    return false;
                }
                sender.sendMessage(ChatColor.GREEN + "Saving complete.");
                return true;
            }
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            return false;
        }
        sendUsageMessage(sender);
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return new ArrayList<>();
    }
}
